﻿using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;



namespace Pract2
{
    internal class Program
    {
        static void Main()
        {
            Stopwatch stopwatch = new();
            stopwatch.Start();
            Hasher.Stopwatch = stopwatch;
            Console.WriteLine("Введите хэш");
            string? hash = Console.ReadLine();
            while (hash is null || hash.Length != 64 || !hash.All(Char.IsLetterOrDigit)) 
            {
                Console.WriteLine("Неверный хэш! Попробуйте снова.");
                hash = Console.ReadLine();
            }
            Hasher.Hash = hash;
            Console.WriteLine("Введите кол-во потоков.");
            int threadKol;
            while (!int.TryParse(Console.ReadLine(), out threadKol) )
            {
                Console.WriteLine("Неверный формат числа. Попробуйте снова.");
            }
            threadKol = (threadKol < 17) && (threadKol > 0) ? threadKol : 16;
            int simKol = Hasher.letters.Length / threadKol;
            for (int i = 0; i < threadKol; i++)
            {
                Hasher hasher = new() { StartIndex = i * simKol };
                if (i == threadKol - 1)
                {
                    hasher.EndIndex = Hasher.letters.Length - 1;
                }
                else
                {
                    hasher.EndIndex = hasher.StartIndex + simKol - 1;
                }
                Thread thread = new(hasher.StartSearch);
                thread.Start();
            }
        }


        public class Hasher
        {
            private static bool _flagFound = false;
            public static Stopwatch? Stopwatch { get; set; }
            public static string Hash { get; set; } = "";
            public int StartIndex { get; init; }
            public int EndIndex { get; set; }

            public static string[] letters = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
                "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
            public void StartSearch()
            {
                string comparingString;
                for (int first = StartIndex; first <= EndIndex; first++)
                {
                    for (int second = 0; second < 26; second++)
                    {
                        for (int third = 0; third < 26; third++)
                        {
                            for (int fourth = 0; fourth < 26; fourth++)
                            {
                                for (int fifth = 0; fifth < 26; fifth++)
                                {
                                    if (_flagFound)
                                    {
                                        Console.WriteLine($"Поток завершил поиск! Он работал на буквах {GetLetters()}.");
                                        return;
                                    }
                                    comparingString = letters[first] + letters[second] + letters[third] + letters[fourth] + letters[fifth];
                                    if (GetResult(comparingString).ToLower() == Hash)
                                    {
                                        Stopwatch!.Stop();
                                        Hasher._flagFound = true;
                                        TimeSpan ts = Stopwatch.Elapsed;
                                        string msecs = ts.Milliseconds.ToString();
                                        if (ts.Milliseconds > 0 && ts.Milliseconds < 10)
                                        {
                                            msecs = "00" + ts.Milliseconds.ToString();
                                        }
                                        else if (ts.Milliseconds > 0 && ts.Milliseconds < 100)
                                        {
                                            msecs = "0" + ts.Milliseconds.ToString();
                                        }
                                        string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.",ts.Hours, ts.Minutes, ts.Seconds)+msecs;
                                        Console.WriteLine($"Поток завершил поиск! Он работал на буквах {GetLetters()}. Этот поток нашёл! Ответ: {comparingString}! Затраченное время на поиск: {elapsedTime}");
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
                Console.WriteLine($"Поток завершил поиск! Он работал на буквах {GetLetters()}.");
            }
            string GetLetters()
            {
                StringBuilder sb = new("");
                for (int i = StartIndex; i <= EndIndex; i++)
                {
                    sb.Append(letters[i]);
                }
                return sb.ToString();
            }
            static string GetResult(string value)
            {
                byte[] hashValue = SHA256.HashData(Encoding.UTF8.GetBytes(value));
                return BitConverter.ToString(hashValue).Replace("-", "");
            }
        }
    }
}